package views;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import utils.DateTool;

@Named(value = "dateToolView")
@RequestScoped
public class DateToolView implements Serializable {

	public String formatDate(Date date) {
		return DateTool.showDate(date);
	}
}
