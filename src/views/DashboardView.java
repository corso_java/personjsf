package views;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import controllers.PersonController;
import model.Person;

@Named
@RequestScoped
public class DashboardView implements Serializable {

	@Inject
	PersonController personController;
	private List<Person> people;
	private List<Person> filteredPeople;
	private Person selected;

	@PostConstruct
	public void init() {
		this.people = personController.getAll();
	}

	public List<Person> getPeople() {
		return people;
	}

	public void setPeople(List<Person> people) {
		this.people = people;
	}

	public List<Person> getFilteredPeople() {
		return filteredPeople;
	}

	public void setFilteredPeople(List<Person> filteredPeople) {
		this.filteredPeople = filteredPeople;
	}

	public Person getSelected() {
		return selected;
	}

	public void setSelected(Person selected) {
		this.selected = selected;
	}

	public String onNewElement() {
		return "createPerson?faces-redirect=true";
	}

	public String onEditElement(Person person) {
		this.selected = person;
		System.out.println(person.toString());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put(Person.class.getSimpleName(),
				selected);
		// FacesContext.getCurrentInstance().getExternalContext().getFlash().keep("person");
		return "createPerson?faces-redirect=true";
	}

	public void onRowSelect(SelectEvent event) {
		this.selected = (Person) event.getObject();
	}

	public void onRowUnselect(UnselectEvent event) {
		this.selected = null;
	}

	public void onRowDblClickSelect(final SelectEvent event) throws IOException {
		this.selected = (Person) event.getObject();
		onEditElement(selected);
		// Questo metodo permette di fare il redirect e modificare l'element del dobbio
		// click
		FacesContext.getCurrentInstance().getExternalContext().redirect("createPerson.xhtml");
		FacesContext.getCurrentInstance().responseComplete();
	}

	public boolean showMenuOptions() {
		return selected != null;
	}

	public void onRemove() {
		// TODO selected.setDeleted(true); personController.store(person);
	}
}
