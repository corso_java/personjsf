package views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import controllers.CarController;
import controllers.PersonController;
import model.Car;
import model.Person;

@Named
@ViewScoped
public class CreatePersonView implements Serializable {

	@Inject
	PersonController personController;
	@Inject
	CarController carController;
	private Person person;
	private List<Car> cars;
	private boolean edit;

	@PostConstruct
	public void init() {
		Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
		edit = flash.containsKey(Person.class.getSimpleName());
		this.cars = carController.getAll();
		if (edit) {
			this.person = (Person) flash.get(Person.class.getSimpleName());
			flash.keep(Person.class.getSimpleName());
		} else {
			this.person = new Person();
		}
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	public boolean canBeDeleted() {
		return edit;
	}

	public String onCompletedSave() {
		personController.store(person);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"La persona " + person.getName() + " è stata salvata correttamente!",
						"Elemento salvato correttamente"));
		// Questa riga mi permette di trasferire il messaggio composto in precedenza,
		// anche nella pagina di arrivo
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		return "dashboard?faces-redirect=true";
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	public List<Car> completeTextCar(String query) {
		String lower = query.toLowerCase();
		List<Car> result = new ArrayList<>();
		for (Car car : cars) {
			if (car.getName().toLowerCase().startsWith(lower) || car.getPlate().startsWith(lower)) {
				result.add(car);
			}
		}
		return result;
	}

}
