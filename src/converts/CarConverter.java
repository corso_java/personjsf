package converts;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import controllers.CarController;
import model.Car;

@FacesConverter(value = "carConverter", forClass = Car.class)
public class CarConverter implements Converter {

	private CarController carController;

	public CarConverter() {
		super();
		try {
			InitialContext ic = new InitialContext();
			carController = (CarController) ic.lookup("java:module/CarController");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			Long carPk = Long.valueOf(value);
			return carController.findByPk(carPk);
		}catch (Exception e) {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return value.toString();
	}

}
