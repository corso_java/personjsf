package dao;

import java.util.List;

public interface IDaoBase<T> {

	public List<T> loadAll();

	public T insert(T entity);

	public T update(T entity);

	public void delete(T entity);

	public T findByPk(Class aClass, Long pk);
}
