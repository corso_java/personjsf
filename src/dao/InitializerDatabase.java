package dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import controllers.CarController;
import model.Car;

@Singleton
@Startup
public class InitializerDatabase {

	@Inject
	CarController carController;

	@PostConstruct
	public void init() {
		System.out.println("Startup database...");
		if (carController.getAll().size() == 0) {
			System.out.println("Creating cars...");
			loadExampleCars();
		}
	}

	private void loadExampleCars() {
		List<Car> cars = new ArrayList<>();
		cars.add(new Car("AB123CD", "Fiat 500"));
		cars.add(new Car("AB124CD", "Smart 2 posti"));
		cars.add(new Car("AB125CD", "Ferrari Dino"));
		cars.add(new Car("CB126CD", "Mercedes classe A"));
		for (Car car : cars) {
			carController.store(car);
		}
	}

}
