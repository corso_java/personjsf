package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class BaseDao<T> {

	@PersistenceContext
	EntityManager em;

	public T insert(T entity) {
		em.persist(entity);
		return entity;
	}

	public T update(T entity) {
		return em.merge(entity);
	}

	public void delete(T entity) {
		em.remove(entity);
	}

	public T findByPk(Class aClass, Long pk) {
		return (T) em.find(aClass, pk);
	}

	public T singleResultQuery(Class aClass, String query, Object... values) {
		try {
			TypedQuery<T> result = em.createQuery(query, aClass);
			for (int i = 0; i < values.length; i++) {
				int position = i + 1;
				result.setParameter(position, values[i]);
			}
			return result.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<T> listResultQuery(Class aClass, String query, Object... values) {
		try {
			TypedQuery<T> result = em.createQuery(query, aClass);
			for (int i = 0; i < values.length; i++) {
				int position = i + 1;
				result.setParameter(position, values[i]);
			}
			return result.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<T> getAll(Class aClass) {
		javax.persistence.Query query = em.createQuery("Select u From " + aClass.getSimpleName() + " u");
		return query.getResultList();
	}
}
