package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTool {

	private static String DATE_PATTERN = "dd/MM/yyyy";
	private static String TIME_PATTERN = "HH:mm:ss";

	public static Calendar format(String date, String time) {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_PATTERN + " " + TIME_PATTERN);
			java.util.Date par = simpleDateFormat.parse(date + " " + time);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(par);
			return calendar;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String showDate(Calendar calendar) {
		if (calendar != null) {
			return new SimpleDateFormat(DATE_PATTERN).format(calendar.getTime());
		} else {
			return null;
		}
	}

	public static String showDate(Date date) {
		if (date != null) {
			return new SimpleDateFormat(DATE_PATTERN).format(date);
		} else {
			return null;
		}
	}

	public static String showTime(Date date) {
		if (date != null) {
			return new SimpleDateFormat(TIME_PATTERN + " " + DATE_PATTERN).format(date);
		} else {
			return null;
		}
	}

	public static String showTime(Calendar calendar) {
		if (calendar != null) {
			return new SimpleDateFormat(TIME_PATTERN + " " + DATE_PATTERN).format(calendar.getTime());
		} else {
			return null;
		}
	}

	public static Calendar retrieveSmall(String small) {
		Integer year = Integer.valueOf("20" + small.substring(0, 2));
		Integer month = Integer.valueOf(small.substring(2, 4)) - 1;
		Integer dayOfMonth = Integer.valueOf(small.substring(4, 6));
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, dayOfMonth);
		return calendar;
	}

	public static Date getLastMonday() {
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return cal.getTime();
	}

	public static Date getFirstYear() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_YEAR, 1);
		return cal.getTime();
	}
}
