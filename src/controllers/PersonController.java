package controllers;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.BaseDao;
import model.Person;

@Stateless
public class PersonController {

	@Inject
	BaseDao<Person> baseDao;

	public Person store(Person person) {
		if (person.getPk() == null) {
			return baseDao.insert(person);
		} else {
			return baseDao.update(person);
		}
	}

	public Person findByFiscalCode(String fiscalCode) {
		return baseDao.singleResultQuery(Person.class, "Select c From Person c Where c.fiscalCode = ?1",
				fiscalCode);
	}

	public Person findByPk(Long pk) {
		return baseDao.findByPk(Person.class, pk);
	}

	public List<Person> getAll() {
		return baseDao.getAll(Person.class);
	}
}
