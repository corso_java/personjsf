package controllers;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.BaseDao;
import model.Car;

@Stateless
public class CarController {

	@Inject
	BaseDao<Car> baseDao;

	public Car store(Car car) {
		if (car.getPk() == null) {
			return baseDao.insert(car);
		} else {
			return baseDao.update(car);
		}
	}

	public Car findByPlate(String plate) {
		return baseDao.singleResultQuery(Car.class, "Select c From Car c Where c.plate = ?1", plate);
	}

	public Car findByPk(Long pk) {
		return baseDao.findByPk(Car.class, pk);
	}

	public List<Car> getAll() {
		return baseDao.getAll(Car.class);
	}
}
