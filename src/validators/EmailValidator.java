package validators;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

@Named
@RequestScoped
public class EmailValidator implements Validator<String> {

	@Override
	public void validate(FacesContext context, UIComponent component, String value) throws ValidatorException {
		FacesMessage facesMessage = null;
		if (value != null) {
			String email = (String) value;
			if (email.length() == 0) {
				facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Inserisci un'email", "Email non valida");
			} else {
				if (!email.contains("@")) {
					facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Inserisci un'email corretta",
							"Indirizzo email senza @");
				} else {
					if (!email.contains(".")) {
						facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Inserisci un'email corretta",
								"Indirizzo email senza dominio");
					}
				}
			}
		} else {
			facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Inserisci un'email", "Email non inserita");
		}
		if (facesMessage != null) {
			throw new ValidatorException(facesMessage);
		}
	}
}
